package id.co.nexsoft.summative4.repository;

import id.co.nexsoft.summative4.entity.Department;
import id.co.nexsoft.summative4.entity.UserData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {
    Department findAllById(int id);
}
