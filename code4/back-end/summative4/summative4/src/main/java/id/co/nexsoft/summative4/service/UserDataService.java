package id.co.nexsoft.summative4.service;

import id.co.nexsoft.summative4.entity.UserData;
import id.co.nexsoft.summative4.repository.UserDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDataService {

    @Autowired
    private UserDataRepository userDataRepository;

    public List<UserData> findAll(){
        return userDataRepository.findAll();
    }

    public UserData findById(int id){
        return userDataRepository.findAllById(id);
    }

    public String saveAll(List<UserData> userDataList){
        userDataRepository.saveAll(userDataList);
        return "Success! Total Saved Data " + userDataList.size();
    }

    public String save(UserData userData){
        userDataRepository.save(userData);
        return "Save Data Success";
    }
}
