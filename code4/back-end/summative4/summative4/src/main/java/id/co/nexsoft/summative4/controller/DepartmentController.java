package id.co.nexsoft.summative4.controller;

import id.co.nexsoft.summative4.entity.Department;
import id.co.nexsoft.summative4.entity.UserData;
import id.co.nexsoft.summative4.service.DepartmentService;
import id.co.nexsoft.summative4.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private UserDataService userDataService;

    @GetMapping("/department")
    @CrossOrigin()
    public List<Department> getAllDepartment(){
        return departmentService.findAll();
    }

    @GetMapping("/department/{id}")
    @CrossOrigin()
    public Department getDepartmentById(@PathVariable(value = "id") int id){
        return departmentService.findById(id);
    }

    @PostMapping("/department")
    @CrossOrigin()
    public String addDepartment(@RequestBody Department department){
        for(UserData userData : department.getUserData()){
            userDataService.save(userData);
        }
        return departmentService.save(department);
    }

    @PostMapping("/departments")
    @CrossOrigin()
    public String addAllDepartment(@RequestBody List<Department> departmentList){
       for(Department department : departmentList){
           for(UserData userData : department.getUserData()){
               userDataService.save(userData);
           }
       }
        return departmentService.saveAll(departmentList);
    }
}
