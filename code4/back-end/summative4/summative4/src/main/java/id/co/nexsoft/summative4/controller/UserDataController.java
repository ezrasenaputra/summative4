package id.co.nexsoft.summative4.controller;

import id.co.nexsoft.summative4.entity.UserData;
import id.co.nexsoft.summative4.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserDataController {

    @Autowired
    private UserDataService userDataService;

    @GetMapping("/userdata")
    public List<UserData> getAllData(){
        return userDataService.findAll();
    }

    @GetMapping("/userdata/{id}")
    public UserData getDataById(@PathVariable(value = "id") int id){
        return userDataService.findById(id);
    }

    @PostMapping("/adduser")
    public String addAllData(@RequestBody List<UserData> userDataList){
        return userDataService.saveAll(userDataList);
    }

}
