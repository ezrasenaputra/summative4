package id.co.nexsoft.summative4.repository;

import id.co.nexsoft.summative4.entity.UserData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDataRepository extends JpaRepository<UserData, Integer> {
        UserData findAllById(int id);
}
