package id.co.nexsoft.summative4.service;

import id.co.nexsoft.summative4.entity.Department;
import id.co.nexsoft.summative4.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public List<Department> findAll(){
        return departmentRepository.findAll();
    }

    public Department findById(int id){
        return departmentRepository.findAllById(id);
    }

    public String saveAll(List<Department> departementList){
        departmentRepository.saveAll(departementList);
        return "Success! Total Saved Data " + departementList.size();
    }

    public String save(Department department){
        departmentRepository.save(department);
        return "Save Data Success";
    }
}
