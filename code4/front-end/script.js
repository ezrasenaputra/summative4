
const getData = async () => {
    // Ambil Data
    fetch('http://localhost:8080/department')
        // Handle success
        .then(result => result.json())
        .then((data) =>  {
            const hasil = data;
            let output = '';
            hasil.forEach(element => {
                element.userData.forEach(user => {
                    output += `      <tr>
                    <td>${user.id}</td>
                    <td><input type="text" class="form-control" form="postForm" name="firstName" value="${user.name}"></td>
                    <td><input type="text" class="form-control" form="postForm" name="lastName" value="${user.lastName}"></td>
                    <td><input type="text" class="form-control" form="postForm" name="email" value="${user.email}"></td>
                    <td><input type="date" class="form-control" form="postForm" name="birthDate" value="${user.birthDate}"></td>
                    <td><input type="text" class="form-control" form="postForm" name="department" value="${element.name}"></td>
                </tr>
    `       
            console.log(user.birthDate);
            document.getElementById('body-table').innerHTML = output;
             })
            });
            dataPost = hasil;
            // console.log(data);
        })
        .catch(err => console.log('Request Failed', err)); // Catch errors
};


const postData = document.querySelector("#postForm");
if(postData){
    postData.addEventListener("submit", function(e){
        submitForm(e, this);
    })
}

const submitForm = async (e, form) => {
    e.preventDefault();
    // const btnSubmit = document.getElementById('btnSubmit');
    const jsonFormData = buildJsonFormData(form);
    const postResponse = await fetch("http://localhost:8080/department", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          "Accept": "application/json",
        },
        body : JSON.stringify(jsonFormData)
    });
    const content = await postResponse;
    console.log(content);
    // console.log(response);
    // console.log(form)
}

const buildJsonFormData = (form) => {
    const formData = new FormData(form);
    const userData = { };
    for(const pair of formData.entries()){
        userData[pair[0]] = pair[1];
    }
    const jsonAll = { "name" : formData.get('department'),
                        "userData" : [{
                            "name" : formData.get('firstName'),
                            "lastName" : formData.get('lastName'),
                            "email" : formData.get('email'),
                            "birthDate" : formData.get('birthDate'),
                        }]
                        
                    }
    console.log(jsonAll);
    return jsonAll;
}

